<?php

namespace Drupal\admin_database\Controller;

use \Drupal\Core\Controller\ControllerBase;

class AdminDatabaseController extends ControllerBase {

  public function index() {
    $databaseInfos = \Drupal::database()->getConnectionOptions();
    $dbHost = $databaseInfos["host"];
    $dbName = $databaseInfos["database"];
    $dbUser = $databaseInfos["username"];

    $modulePathDir = \Drupal::service('extension.list.module')->getPath('admin_database') . "/assets";
    $token = \Drupal::state()->get("admin_database_adminer_token");
    $adminerFile = "adminer_$token.php";

    setcookie("admin_database_adminer_file", $adminerFile, 0, "/");

    $src = "/$modulePathDir/adminer_with_plugins.php?server=$dbHost&username=$dbUser&db=$dbName";

    return [
        '#theme' => 'iframe',
        '#src' => $src,
    ];
  }

}
