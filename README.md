# Admin Database

## About this module
This module add an IHM to access to Drupal database with an embed adminer inside back-office.
You can manage permission to use this feature. Be careful users with this permission can have access to database, use with caution !

## Installation
You can download this module with composer, and enable it like any other module.
After that, you can access to your Drupal database here : /admin/admin-db