<?php

    // check adminer path
    if(!array_key_exists("admin_database_adminer_file", $_COOKIE)) {
        die("Adminer not found !");
    }
    $adminerFile = $_COOKIE["admin_database_adminer_file"];
    if(empty($adminerFile)) {
        die("Adminer not found !");
    }    

    function adminer_object() {
        // required to run any plugin
        include_once "./plugins/plugin.php";
        
        // autoloader
        foreach (glob("plugins/*.php") as $filename) {
            include_once "./$filename";
        }
        
        // enable extra drivers just by including them
        //~ include "./plugins/drivers/simpledb.php";


        
        $plugins = array(
            // specify enabled plugins here
            new AdminerFrames(),
        );
        
        /* It is possible to combine customization and plugins:
        class AdminerCustomization extends AdminerPlugin {
        }
        return new AdminerCustomization($plugins);
        */
        
        return new AdminerPlugin($plugins);
    }


    // include original Adminer or Adminer Editor
    include $adminerFile;

?>
